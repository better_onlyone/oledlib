/*
	原作者哔哩哔哩:							MjGame 		https://space.bilibili.com/38673747
	同GifHub:								maoyongjie 	https://github.com/hello-myj/stm32_oled/
	代码整理注释删减增加优化等 哔哩哔哩:	一只程序缘	https://space.bilibili.com/237304109
	整理之前的原代码随本代码一同提供,浏览以上网址获取更多相关信息,本代码以征得原作同意,感谢原作
	
	图形库操作说明: 请转移至test.c顶部
*/

#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "beep.h"
#include "timer.h"
#include "test.h"
#include "draw_api.h"

u8 flag1s;

int main(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	delay_init();
	BEEP_Init();
	uart_init(115200);
	TIM3_Int_Init(10-1,7200-1);
	InitGraph();
	
	while(1)
	{
		demo();
	}
} 
