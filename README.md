# oledlib

#### 介绍
一个开源的oled图形库

#### 软件架构
此图形库采用STM32F1单片机
0.96寸的spi通信方式oled
编写的一个oled图形库

#### 安装教程

1.  若编译出现问题 文件路径下请不要出现中文字符

#### 使用说明

1.  在每个c文件开头都有相关注释
2.  若对此代码有什么不理解之处 可见网址 https://www.bilibili.com/video/BV1EC4y1872W 有代码的测试效果极其简要分析

#### 参与贡献

该代码由原作写出 经我整理并由原作同意后发出分享给大家,如果你有更好的想法,欢迎在此提交分支,我们一起完善这个图形库
1.  原作bilibili:MjGame
2.  原作视频:https://www.bilibili.com/video/BV1MV411o7P5
3.  原作代码开源地址:https://github.com/hello-myj/stm32_oled
